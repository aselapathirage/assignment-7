/*
    Asela Pathirage
    Index no: 19020538

    Program to reverse a sentence given by user
*/

#include <stdio.h>
#include <string.h>

#define SIZE 1000

int main(){
    char sen[SIZE+1];
    /* Getting a sentence as user input*/
    puts("Enter a sentence: ");
    gets(sen);
    /*calculating the length of the string*/
    int n=strlen(sen);
    /*reversing the sentence*/
    for(int i=0, j=n-1 ; i<j ; i++, j--){
        char temp=sen[i];
        sen[i]=sen[j];
        sen[j]=temp;
    }
    /*Printing the reversed sentence*/
    puts("Reversed sentence: ");
    puts(sen);

    return 0;
}
