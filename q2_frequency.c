/*
    Asela Pathirage
    Index no: 19020538

    Program to find the Frequency of a given character given by Command Line Arguments
    <program name> <character> <sentence>
*/
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){
    char ch=argv[1][0];
    int freq=0;

    /*Calculating the frequency of the give character*/
    for(int i=2; i<argc; i++){
        for (int j = 0; j<strlen(argv[i]); j++)
            if(argv[i][j]==ch) freq++;
    }
    /*printing the frequency*/
    printf("Frequency of '%c' in given sentence: %d\n", ch, freq);

    return 0;
}
